$(function() {

	var Menu = {

	    sel: {
	        menu: '.menu',
	        title: '.menu__item-link',
            menuLeft: '.menu__left',
            menuRight: '.menu__right'
        },

        classes: {
	        active: 'active'
        },

	    init: function () {
            this.load();
        },

        load: function () {
	        var self = this;

	        $(document).on('click', this.sel.title, function (e) {
	            e.preventDefault();

	            if (!$(this).hasClass(self.classes.active)) {
	                self.change($(this));
                }

            });
        },

        change: function (el) {
            Menu.hide(el);
            Menu.show(el);
        },

        hide: function (el) {
            el.parents(Menu.sel.menu).find('.' + Menu.classes.active).removeClass(Menu.classes.active);
        },

        show: function (el) {
            var id = el.attr('href');
            var content = $(id);
            el.addClass(Menu.classes.active);
            content.addClass(Menu.classes.active);
            content.parents(Menu.sel.menuRight).scrollTop(0);
            var width = $(window).width();
            if (width <= 740) {
                $('body, html').animate({scrollTop: Menu.toScroll(el)}, 500);
            }
        },

        toScroll: function (el) {
	        var parent = el.parents(Menu.sel.menuLeft);
            var top = parent.offset().top;
            var height = parent.height();
            return top + height;
        }
    };
    window.Menu = Menu;
	Menu.init();

    $(document).on('af_complete', function(event, response) {
        var form = response.form;
        if (form.attr('id') === 'callback') {
            form.trigger('reveal:close');
        }
        else {
            console.log(response)
        }
    });

});
