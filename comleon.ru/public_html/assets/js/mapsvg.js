// mapSVG v5.2.3 - 27 September 2012
// http://codecanyon.net/user/Yatek/portfolio
(function (a) {
    var b = navigator.userAgent.toLowerCase();
    var c = b.indexOf("ipad") > -1 || b.indexOf("iphone") > -1 || b.indexOf("ipod") > -1 || b.indexOf("android") > -1;
    var d = document.getElementsByTagName("script");
    var e = d[d.length - 1].src.split("/");
    e.pop();
    var f = e.join("/") + "/";
    e.pop();
    var g = e.join("/") + "/";
    var h;
    var i = false;
    var j, k, l, m, n, o, p, q, r, s, t, u = {};
    var v;
    var w = {};
    var x;
    var y = 1;
    var z = 0;
    var A = {};
    var B = {};
    var C = [];
    var D = false;
    var E = [];
    var F = [];
    var G = [];
    var H = 0;
    var I = {};
    var J;
    var K = a.browser.msie ? function (b) {
        return {
            x: b.clientX + a("html").scrollLeft(),
            y: b.clientY + a("html").scrollTop()
        }
    } : function (a) {
        return {
            x: a.pageX,
            y: a.pageY
        }
    };
    var L = [128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824, 2147483648, 4294967296, 8589934592, 17179869184, 34359738368, 68719476736, 137438953472];
    var M = [.7111111111111111, 1.4222222222222223, 2.8444444444444446, 5.688888888888889, 11.377777777777778, 22.755555555555556, 45.51111111111111, 91.02222222222223, 182.04444444444445, 364.0888888888889, 728.1777777777778, 1456.3555555555556, 2912.711111111111, 5825.422222222222, 11650.844444444445, 23301.68888888889, 46603.37777777778, 93206.75555555556, 186413.51111111112, 372827.02222222224, 745654.0444444445, 1491308.088888889, 2982616.177777778, 5965232.355555556, 11930464.711111112, 23860929.422222223, 47721858.844444446, 95443717.68888889, 190887435.37777779, 381774870.75555557, 763549741.5111111];
    var N = [40.74366543152521, 81.48733086305042, 162.97466172610083, 325.94932345220167, 651.8986469044033, 1303.7972938088067, 2607.5945876176133, 5215.189175235227, 10430.378350470453, 20860.756700940907, 41721.51340188181, 83443.02680376363, 166886.05360752725, 333772.1072150545, 667544.214430109, 1335088.428860218, 2670176.857720436, 5340353.715440872, 10680707.430881744, 21361414.86176349, 42722829.72352698, 85445659.44705395, 170891318.8941079, 341782637.7882158, 683565275.5764316, 1367130551.1528633, 2734261102.3057265, 5468522204.611453, 10937044409.222906, 21874088818.445812, 43748177636.891624];
    var O = {
        keepSourceStyles: false,
        loadingText: "Loading map...",
        colors: {
            base: "#E1F1F1",
            background: "#eeeeee",
            hover: "#548eac",
            selected: "#065A85",
            disabled: "#ffffff",
            stroke: "#7eadc0"
        },
        regions: {},
        viewBox: [],
        cursor: "default",
        strokeWidth: 1,
        scale: 2,
        tooltipsMode: "hover",
        tooltips: {
            show: "hover",
            mode: "names"
        },
        onClick: null,
        mouseOver: null,
        mouseOut: null,
        disableAll: false,
        hideAll: false,
        marks: null,
        hover_mode: "color",
        selected_mode: "color",
        hover_brightness: 1.3,
        selected_brightness: 3,
        pan: false,
        zoom: false,
        popover: {
            width: "auto",
            height: "auto"
        },
        buttons: true,
        zoomLimit: [0, 5],
        zoomDelta: 1.2,
        zoomButtons: {
            show: true,
            location: "right"
        }
    };
    t = {
        width: 32,
        height: 32,
        attrs: {
            cursor: "pointer",
            src: g + "/markers/_pin_default.png"
        },
        label: {
            attrs: {
                fill: "#000000",
                "font-size-abs": 13
            }
        }
    };
    var P = {
        destroy: function () { },
        getScale: function () {
            var a = u.width / u.viewBox[2];
            var b = l.width / E[2];
            var c = 1 - (a - b);
            return c
        },
        getViewBox: function () {
            return F
        },
        setViewBox: function (a, b) {
            var c = a && a.length == 4 ? a : u.viewBox;
            var d = [parseFloat(c[0]), parseFloat(c[1]), parseFloat(c[2]), parseFloat(c[3])];
            F = d;
            p.setViewBox(F[0], F[1], F[2], F[3], true);
            P.marksAdjustPosition();
            P.mapAdjustStrokes();
            return true
        },
        viewBoxSetBySize: function (b, c) {
            E = P.viewBoxGetBySize(b, c);
            F = a.extend([], E);
            x = P.getScale();
            p.setViewBox(F[0], F[1], F[2], F[3], true);
            P.marksAdjustPosition();
            return F
        },
        viewBoxGetBySize: function (a, b) {
            var c = a / b;
            var d = u.viewBox[2] / u.viewBox[2];
            var e = u.viewBox;
            var f = u.width / e[2];
            var g = u.height / e[3];
            var h = a / f;
            var i = b / g;
            if (c != d) {
                if (c > 1) {
                    e[2] = e[3] * c
                } else {
                    e[3] = e[2] / c
                }
            }
            return e
        },
        viewBoxReset: function () {
            P.setViewBox()
        },
        mapAdjustStrokes: function () {
            var a = x;
            q.attr({
                "stroke-width": w.strokes["stroke-width"] / a
            })
        },
        zoomIn: function () {
            P.zoom(1)
        },
        zoomOut: function () {
            P.zoom(-1)
        },
        zoom: function (a, b, c) {
            var d = a > 0 ? 1 : -1;
            var e = H;
            e += d;
            if (e > l.zoomLimit[1] || e < l.zoomLimit[0]) return false;
            H = e;
            var f = d * l.zoomDelta;
            if (f < 1) f = -1 / f;
            var g = F[2];
            var h = F[3];
            x = x * f;
            y = y * f;
            J = y / f;
            var i = [];
            i[2] = E[2] / y;
            i[3] = E[3] / y;
            i[0] = F[0] + (g - i[2]) / 2;
            i[1] = F[1] + (h - i[3]) / 2;
            P.setViewBox(i, true)
        },
        markUpdate: function (b, c) {
            if (c.attrs["src"] == "") delete c.attrs["src"];
            if (c.attrs["href"] == "") delete c.attrs["href"];
            var d = a.extend(true, {}, b.data(), t, c);
            b.data(d);
            if (l.editMode && c.attrs.href) {
                var e = c.attrs.href;
                delete c.attrs.href
            }
            b.attr(c.attrs);
            if (e) b.data("href", e)
        },
        markDelete: function (a) {
            a.remove()
        },
        markAdd: function (b, c) {
            var d = a.extend(true, {}, t, b);
            var e = d.xy ? d.xy : d.attrs.x ? [d.attrs.x, d.attrs.y] : d.c ? P.ll2px(d.c) : false;
            if (d.c) {
                e[0] = e[0] - t.width / (2 * x);
                e[1] = e[1] - t.height / x
            }
            if (!e) return false;
            e[0] = parseInt(e[0]);
            e[1] = parseInt(e[1]);
            d.width = t.width;
            d.height = t.height;
            if (c) {
                e[0] = (e[0] - d.width / 2) / x;
                e[1] = (e[1] - d.height) / x;
                var f = d.width / x;
                var g = d.height / x
            } else {
                var f = d.width;
                var g = d.height
            }
            var h = a.extend(true, {}, d.attrs);
            if (l.editMode) delete h.href;
            var i = p.image(d.attrs.src, e[0], e[1], f, g).attr(h).data(d);
            if (!l.editMode) {
                i.mousedown(function (a) {
                    if (this.data("popover")) {
                        var b = K(a);
                        P.showPopover(b.x, b.y, this.data("popover"))
                    }
                })
            }
            if (l.editMode && c) {
                l.marksEditHandler.call(i);
                P.setMarksEditMode(true, i)
            }
            r.push(i);
            return i
        },
        marksAdjustPosition: function () {
            if (!r) return false;
            var a = t.width / 2 - t.width / (2 * x);
            var b = t.height - t.height / x;
            r.attr({
                width: t.width / x,
                height: t.height / x
            }).transform("t" + a + "," + b)
        },
        markGetDefaultCoords: function (a, b, c, d, e) {
            c = parseInt(c);
            d = parseInt(d);
            var f = c / (2 * e) - c / 2;
            a += c / (2 * e) - c / 2;
            b += d / e - d;
            return [a, b]
        },
        markMoveStart: function () {
            this.data("ox", parseFloat(this.attr("x")));
            this.data("oy", parseFloat(this.attr("y")))
        },
        markMove: function (a, b) {
            a = a / x;
            b = b / x;
            this.attr({
                x: this.data("ox") + a,
                y: this.data("oy") + b
            })
        },
        markMoveEnd: function () {
            if (this.data("ox") == this.attr("x") && this.data("oy") == this.attr("y")) {
                l.marksEditHandler.call(this)
            }
        },
        panStart: function (b) {
            if (b.target.id == "btnZoomIn" || b.target.id == "btnZoomOut") return false;
            b.preventDefault();
            I.dx = F[0];
            I.dy = F[1];
            I.x = b.clientX;
            I.y = b.clientY;
            a("body").on("mousemove", P.panMove).on("mouseup", P.panEnd)
        },
        panMove: function (b) {
            b.preventDefault();
            q.attr({
                cursor: "move"
            });
            a("body").css({
                cursor: "move"
            });
            var c = I.x - b.clientX;
            var d = I.y - b.clientY;
            I.dx = F[0] + c / x;
            I.dy = F[1] + d / x;
            p.setViewBox(I.dx, I.dy, F[2], F[3], true)
        },
        panEnd: function (b) {
            a("body").css({
                cursor: "default"
            });
            q.attr({
                cursor: l.cursor
            });
            F[0] = I.dx;
            F[1] = I.dy;
            a("body").off("mousemove", P.panMove).off("mouseup", P.panEnd)
        },
        marksHide: function () {
            r.hide()
        },
        marksShow: function () {
            r.show()
        },
        marksGet: function () {
            var b = [];
            a.each(r, function (c, d) {
                if (d.attrs) {
                    var e = a.extend({}, d.attrs);
                    if (!d.data("placed")) {
                        var f = P.markGetDefaultCoords(d.attrs.x, d.attrs.y, d.data("width"), d.data("height"), x);
                        e.x = f[0];
                        e.y = f[1]
                    }
                    if (d.data("href")) e.href = d.data("href");
                    b.push({
                        attrs: e,
                        tooltip: d.data("tooltip"),
                        popover: d.data("popover"),
                        width: d.data("width"),
                        height: d.data("height"),
                        href: d.data("href"),
                        placed: true
                    })
                }
            });
            return b
        },
        selectRegion: function (a) {
            if (!B[a] || B[a].disabled) return false;
            if (z) B[z].attr(B[z].default_attr);
            z = a;
            switch (l.selected_mode) {
                case "color":
                    B[a].attr(w.selected);
                    break;
                case "brightness":
                    B[a].attr({
                        fill: P.lighten(B[a].default_attr.fill, l.selected_brightness)
                    });
                    break;
                default:
                    null
            }
        },
        unhighlightRegion: function (a) {
            if (B[a].disabled || z == a) return false;
            B[a].attr({
                fill: B[a].default_attr.fill
            })
        },
        highlightRegion: function (a) {
            if (B[a].disabled || z == a) return false;
            switch (l.hover_mode) {
                case "color":
                    B[a].attr(w.hover);
                    break;
                case "brightness":
                    B[a].attr({
                        fill: P.lighten(B[a].default_attr.fill, l.hover_brightness)
                    });
                    break;
                default:
                    null
            }
        },
        ll2px: function (a) {
            var b = a[0];
            var c = a[1];
            var d = 2;
            var e = L[d];
            var f = l.width / 1024;
            var f = 1;
            var g = Math.round(e + c * M[d]);
            var h = Math.sin(b * 3.14159 / 180);
            if (h < -.9999) h = -.9999;
            else if (h > .9999) h = .9999;
            var i = Math.round(e + .5 * Math.log((1 + h) / (1 - h)) * -N[d]);
            var j = [f * (g - 32.5), f * (i - 142)];
            return j
        },
        isRegionDisabled: function (a, b) {
            if (l.regions[a]) {
                if (l.regions[a].disabled || b == "none") return true;
                else return false
            } else if (l.disableAll || b == "none") {
                return true
            } else {
                return false
            }
        },
        regionClickHandler: function (a) {
            if (l.pan && (Math.abs(F[0] - I.dx) > 5 || Math.abs(F[1] - I.dy) > 5)) return false;
            P.selectRegion(this.name);
            var b = K(a);
            if (this.popover) P.showPopover(b.x, b.y, this.popover);
            if (l.onClick) return l.onClick.call(this, a, P)
        },
        addText: function (b) {
            var c = P.getElementStyles(b);
            var d = a.map(a(b).find("tspan"), function (b, c) {
                return a(b).text()
            });
            var e = parseInt(c.getPropertyValue("font-size"));
            var f = d.length > 1 ? -e * d.length / (2 * 1.5) : e / 2;
            d = d.join("\n");
            var g = a(b).attr("x");
            var h = a(b).attr("y") - f;
            var i = p.text(g, h, d).attr({
                "font-size": e,
                "font-family": c.getPropertyValue("font-family") + ", sans-serif, verdana",
                "font-weight": c.getPropertyValue("font-weight"),
                fill: c.getPropertyValue("fill"),
                "text-anchor": "start"
            });
            a(i.node).css({
                "-webkit-touch-callout": "none",
                "-webkit-user-select": "none",
                cursor: "default",
                "pointer-events": "none"
            })
        },
        getElementStyles: function (b) {
            if (!a.browser.msie) {
                return b.style
            } else {
                var c = a(b).attr("style").split(";");
                var d = {};
                a.each(c, function (a, b) {
                    var c = b.split(":");
                    d[c[0]] = c[1]
                });
                d.getPropertyValue = function (a) {
                    return d[a]
                };
                return d
            }
        },
        regionAdd: function (b) {
            var c = P.getElementStyles(b);
            var d = c.getPropertyValue("fill");
            var e = a(b).attr("id");
            B[e] = p.path(a(b).attr("d"));
            B[e].name = e;
            B[e].disabled = P.isRegionDisabled(e, d);
            B[e].default_attr = {};
            if (l.keepSourceStyles) {
                B[e].default_attr["fill"] = c.getPropertyValue("fill");
                B[e].default_attr["fill-opacity"] = c.getPropertyValue("fill-opacity");
                B[e].default_attr["stroke-color"] = c.getPropertyValue("stroke-color");
                B[e].default_attr["stroke-width"] = c.getPropertyValue("stroke-width");
                var f = c.getPropertyValue("stroke-dasharray");
                if (f && f != "none") B[e].default_attr["stroke-dasharray"] = "--"
            } else {
                if (d && d != "none") B[e].default_attr = B[e].disabled ? w.disabled : w.def;
                else B[e].default_attr = {}
            }
            if (B[e].disabled) B[e].default_attr.cursor = "defalut";
            else B[e].default_attr.cursor = l.cursor;
            if (l.regions[e]) {
                if (l.regions[e].attr) B[e].default_attr = a.extend(true, {}, B[e].default_attr, l.regions[e].attr);
                if (l.regions[e].tooltip) B[e].tooltip = l.regions[e].tooltip;
                if (l.regions[e].popover) B[e].popover = l.regions[e].popover;
                if (l.regions[e].selected) P.selectRegion(e)
            }
            B[e].attr(B[e].default_attr);
            q.push(B[e])
        },
        lighten2: function (a, b) {
            var c = a.charAt(0) == "#" ? a.substring(1, 7) : a;
            var d = Raphael.rgb2hsb(parseInt(c.substring(0, 2), 16), parseInt(c.substring(2, 4), 16), parseInt(c.substring(4, 6), 16));
            d.b += .1;
            console.log(a, d);
            return Raphael.hsb(d)
        },
        lighten: function (a, b) {
            if (b < 0) b = 0;
            var c = a;
            if (c.substr(0, 1) == "#") c = c.substring(1);
            if (c.length == 3 || c.length == 6) {
                var d = c.length / 3;
                var e;
                var f = parseInt(c.substr(0, d), 16);
                e = b * f / (256 - f);
                f = Math.floor(256 * e / (e + 1));
                f = f.toString(16);
                if (f.length == 1) f = "0" + f;
                var g = parseInt(c.substr(d, d), 16);
                e = b * g / (256 - g);
                g = Math.floor(256 * e / (e + 1));
                g = g.toString(16);
                if (g.length == 1) g = "0" + g;
                var h = parseInt(c.substr(2 * d, d), 16);
                e = b * h / (256 - h);
                h = Math.floor(256 * e / (e + 1));
                h = h.toString(16);
                if (h.length == 1) h = "0" + h;
                c = f + g + h
            }
            return "#" + c
        },
        setPan: function (a) {
            if (a) {
                l.pan = true;
                j.on("mousedown", P.panStart)
            } else {
                if (l.pan) j.off("mousedown", P.panStart);
                l.pan = false
            }
        },
        markAddClickHandler: function (a) {
            P.markAdd({
                xy: [a.offsetX, a.offsetY]
            }, true)
        },
        setMarksEditMode: function (a, b) {
            a = P.parseBoolean(a);
            var c = b ? b : r;
            if (a) {
                if (l.editMode === false) {
                    c.unhover()
                }
                if (!b) q.click(P.markAddClickHandler);
                c.drag(P.markMove, P.markMoveStart, P.markMoveEnd)
            } else {
                if (l.editMode) {
                    if (!b) q.unclick(P.markAddClickHandler);
                    c.undrag()
                }
                c.hover(function () {
                    if (this.data("tooltip")) {
                        n.html(this.data("tooltip"));
                        n.show()
                    }
                }, function () {
                    if (this.data("tooltip")) n.hide()
                })
            }
            l.editMode = a
        },
        setZoom: function (b) {
            if (b) {
                l.zoom = true;
                j.bind("mousewheel.mapsvg", function (a, b, c, d) {
                    var e = b > 0 ? 1 : -1;
                    P.zoom(e, a.offsetX, a.offsetY);
                    return false
                });
                if (l.zoomButtons.show) {
                    var c = a("<div></div>");
                    var d = {
                        "border-radius": "3px",
                        display: "block",
                        "margin-bottom": "7px"
                    };
                    var e = a('<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABhElEQVR4nJWTT4rqQBDGf92pSEJWmYfgQpABb+EB1NU8DyBe5M1q5iKStTCDd/AWggElC3EQJAQxbb/NJDH+mccraEh31fdVfR8pBRBF0Uuapn+AX8CZn0MDuyAI3sfj8aeaTqcvWZZ9XFdZazmdTgC4rotS6oYpCILfkmXZ6yNwt9tFKcVyucRxnBuSNE1fNfB0TWCModlsMhwOGQwGdDod8jy/J+dJP9JsjKl9W2vvlZ3lcuyiS57ntY7FvZDgum6Zk0vN7XYbay3GGMIwLItarRbGGEQErTVxHON5XkVQAEaj0b0x6fV6tXsURRwOBxzHQd9F/CPO58o2ARARdrsds9ms9CIMQ/r9PgCLxYL1eo3rulhr2e/3dQkAnueRJElp2vF4LLskScJmsynNK8A1AqjcVUohUqVEBBGpuV+E/j63CV093/sLizIBvoDny1fHcdhut8znc5RSrFar2kQX8aV933+7ZldK0Wg0iOO4BD9YpjcF8L2R/7XOvu+/TyaTz79+UqnWsVHWHAAAAABJRU5ErkJggg==" id="btnZoomIn"/>').on("click", function (a) {
                        P.zoomIn()
                    }).css(d);
                    var f = a('<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA6klEQVR4nKWTPW6DQBBG3w4RaLXSFs4puAe9fQHEReLKPgYN4gLxQei5RNytFraANNEKKwk29uum+N78SKMA2rbdO+c+gHdgYh0Bvowx57IsL6ppmr33/vNO6E+MMQfx3h+fCQM4544C7J4VADvh/s5rTG/LKoTANK37RIQ0TWMdBSEE8jwnyzLmef437L2n7/soiQLnHEVRPDR313VRIA8lVogTWGup6/pmhSRJAFBKxcAwDFhrfwuSJCGEwDiOqx2VUlF8I1h23ILw2h1EgOsLgqtorU/LI23BGHNSAD8fuemdtdbnqqou39SbTK6RdYDsAAAAAElFTkSuQmCC" id="btnZoomOut"/>').on("click", function (a) {
                        P.zoomOut()
                    }).css(d);
                    c.append(f).append(e).css({
                        position: "absolute",
                        top: "15px",
                        width: "16px",
                        cursor: "pointer"
                    });
                    if (l.zoomButtons.location == "right") c.css({
                        right: "15px"
                    });
                    else if (l.zoomButtons.location == "left") c.css({
                        left: "15px"
                    });
                    j.append(c)
                }
            } else {
                l.zoom = false;
                j.unbind("mousewheel.mapsvg")
            }
        },
        setSize: function (b, c, d) {
            l.width = parseInt(b);
            l.height = parseInt(c);
            l.responsive = parseInt(d);
            if (!l.width && l.height) {
                l.width = l.height * u.width / u.height
            } else if (l.width && !l.height) {
                l.height = l.width * u.height / u.width
            } else if (!l.width && !l.height) {
                l.width = u.width;
                l.height = u.height;
                D = true
            }
            h = l.width / l.height;
            x = P.getScale();
            j.height(j.width() / h);
            if (a.browser.msie && a.browser.version < 9) d = false;
            if (d) {
                j.css({
                    "max-width": l.width + "px",
                    "max-height": l.height + "px",
                    width: "auto",
                    height: "auto",
                    position: "relative"
                }).height(j.width() / h);
                a(window).bind("resize.mapsvg", function () {
                    j.height(j.width() / h)
                })
            } else {
                j.css({
                    width: l.width + "px",
                    height: l.height + "px",
                    "max-width": "none",
                    "max-height": "none",
                    position: "relative"
                });
                a(window).unbind("resize.mapsvg")
            }
            return [l.width, l.height]
        },
        setMarks: function (b) {
            if (b) {
                a.each(b, function (a, b) {
                    P.markAdd(b)
                });
                P.marksAdjustPosition()
            }
        },
        showTip: function () { },
        showPopover: function () { },
        hideTip: function () {
            n.hide();
            n.html("")
        },
        setTooltip: function (b) {
            a("body").prepend('<div id="map_tooltip"></div>');
            n = a("#map_tooltip");
            n.css({
                "font-size": "12px",
                "font-weight": "normal",
                position: "absolute",
                "border-radius": "4px",
                "-moz-border-radius": "4px",
                "-webkit-border-radius": "4px",
                top: "0",
                left: "0",
                "z-index": "1000",
                display: "none",
                "background-color": "white",
                border: "1px solid #eee",
                padding: "4px 7px"
            });
            if (l.tooltips.show == "hover") {
                j.mousemove(function (b) {
                    a("#map_tooltip").css("left", b.pageX).css("top", b.pageY + 30)
                })
            }
            P.showTip = l.tooltipsMode == "custom" ? function (a) {
                if (B[a].tooltip) {
                    n.html(B[a].tooltip);
                    n.show()
                }
            } : l.tooltipsMode == "names" ? function (a) {
                if (B[a].disabled) return false;
                n.html(a.replace(/_/g, " "));
                n.show()
            } : l.tooltipsMode == "combined" ? function (a) {
                if (B[a].tooltip) {
                    n.html(B[a].tooltip);
                    n.show()
                } else {
                    if (B[a].disabled) return false;
                    n.html(a.replace(/_/g, " "));
                    n.show()
                }
            } : function (a) {
                null
            }
        },
        setPopover: function (b) {
            if (!b) return false;
            //a("body").prepend('<div id="map_popover"><div id="map_popover_content"></div><div id="map_popover_close">x</div></div>');
            a("#map").prepend('<div id="map_popover"><div id="map_popover_content"></div><div id="map_popover_close">x</div></div>');
            o = a("#map_popover");
            var c = o.find("#map_popover_close");
            o.css({
                "font-size": "12px",
                "font-weight": "normal",
                position: "absolute",
                "border-radius": "4px",
                "-moz-border-radius": "4px",
                "-webkit-border-radius": "4px",
                top: "0",
                left: "0",
                "z-index": "1000",
                width: l.popover.width + (l.popover.width == "auto" ? "" : "px"),
                height: l.popover.height + (l.popover.height == "auto" ? "" : "px"),
                display: "none",
                "background-color": "white",
                border: "1px solid #ccc",
                padding: "12px",
                "-webkit-box-shadow": "5px 5px 5px 0px rgba(0, 0, 0, 0.2)",
                "box-shadow": "5px 5px 5px 0px rgba(0, 0, 0, 0.2)"
            });
            c.css({
                position: "absolute",
                top: "0",
                right: "5px",
                cursor: "pointer",
                color: "#aaa",
                "z-index": "1200"
            });
            P.showPopover = function (b, c, d) {
                if (d) {
                    o.find("#map_popover_content").html(d);
                    //var e = b - o.outerWidth() / 2;
                    //var f = c - o.outerHeight() - 7;
                    var e = 0;
                    var f = 0;
                    if (e < 0) e = 0;
                    if (f < 0) f = 0;
                    //if (e + o.outerWidth() > a(window).width()) e = a(window).width() - o.outerWidth();
                    //if (f + o.outerHeight() > a(window).width()) f = a(window).height() - o.outerHeight();
                    a("#map_popover").css("left", e).css("top", f);
                    console.log(a(window).height() - o.outerHeight());
                    o.show()
                }
            };
            P.hidePopover = function () {
                o.find("#map_popover_content").html("");
                o.hide()
            };
            c.on("click", P.hidePopover)
        },
        popoverOffHandler: function (b) {
            if (!a(b.target).closest("#map_popover").length) P.hidePopover()
        },
        parseBoolean: function (a) {
            switch (String(a).toLowerCase()) {
                case "true":
                case "1":
                case "yes":
                case "y":
                    return true;
                case "false":
                case "0":
                case "no":
                case "n":
                    return false;
                default:
                    return undefined
            }
        },
        mouseOverHandler: function () { },
        mouseOutHandler: function () { },
        mouseDownHandler: function () { },
        init: function (b) {
            if (!b.source) {
                alert("mapSVG Error: Please provide a map URL");
                return false
            }
            j = a(this);
            b.pan = P.parseBoolean(b.pan);
            b.zoom = P.parseBoolean(b.zoom);
            b.responsive = P.parseBoolean(b.responsive);
            b.disableAll = P.parseBoolean(b.disableAll);
            l = a.extend(true, O, b);
            j.css({
                background: l.colors.background
            });
            var d = a("<div>" + l.loadingText + "</div>").css({
                position: "absolute",
                top: "50%",
                left: "50%",
                "z-index": -1,
                padding: "10px 15px",
                "border-radius": "10px",
                background: "#eee",
                display: "none"
            });
            j.append(d);
            w = {
                strokes: {
                    stroke: l.colors.stroke,
                    "stroke-width": l.strokeWidth,
                    "stroke-linejoin": "round"
                },
                def: {
                    fill: l.colors.base
                },
                disabled: {
                    fill: l.colors.disabled
                },
                hover: {
                    fill: l.colors.hover
                },
                selected: {
                    fill: l.colors.selected
                }
            };
            a.ajax({
                url: l.source,
                contentType: "image/svg+xml; charset=utf-8",
                beforeSend: function (a) {
                    if (a && a.overrideMimeType) {
                        a.overrideMimeType("image/svg+xml; charset=utf-8")
                    }
                },
                success: function (b) {
                    data = b;
                    var d = a(data).find("svg");
                    u.width = parseFloat(d.attr("width").replace(/px/g, ""));
                    u.height = parseFloat(d.attr("height").replace(/px/g, ""));
                    u.viewBox = d.attr("viewBox") ? d.attr("viewBox").split(" ") : [0, 0, u.width, u.height];
                    a.each(u.viewBox, function (a, b) {
                        u.viewBox[a] = parseInt(b)
                    });
                    E = l.viewBox.length == 4 ? l.viewBox : u.viewBox;
                    P.setSize(l.width, l.height, l.responsive);
                    if (a.browser.msie && a.browser.version < 9) p = Raphael(j.attr("id"), l.width, l.height);
                    else p = Raphael(j.attr("id"), "100%", "100%");
                    q = p.set();
                    r = p.set();
                    P.setViewBox(E);
                    a("path", data).each(function (a, b) {
                        P.regionAdd(b)
                    });
                    a("text", data).each(function (a, b) {
                        P.addText(b)
                    });
                    w.strokes["stroke-width"] /= x;
                    q.attr(w.strokes);
                    P.setPan(l.pan);
                    P.setZoom(l.zoom);
                    P.setMarks(l.marks);
                    P.setMarksEditMode(l.editMode);
                    P.setTooltip(l.tooltips.mode);
                    P.setPopover(l.popover);
                    var e = "";
                    if (!c) {
                        e = "methods.highlightRegion(this.name);";
                        if (l.tooltips.show == "hover") e += "methods.showTip(this.name);";
                        if (l.mouseOver) e += "return options.mouseOver.call(this, e, methods);";
                        P.mouseOverHandler = new Function("e, methods, options", e);
                        e = "";
                        e += "methods.unhighlightRegion(this.name);";
                        if (l.tooltips.show == "hover") e += "methods.hideTip();";
                        if (l.mouseOut) e += "return options.mouseOut.call(this, e, methods);";
                        P.mouseOutHandler = new Function("e, methods, options", e)
                    }
                    e = "";
                    e = "methods.regionClickHandler.call(this, e);";
                    P.mouseDownHandler = new Function("e, methods", e);
                    if (!c) q.mouseover(function (a) {
                        P.mouseOverHandler.call(this, a, P, l)
                    }).mouseout(function (a) {
                        P.mouseOutHandler.call(this, a, P, l)
                    });
                    if (!l.pan) q.mousedown(function (a) {
                        P.mouseDownHandler.call(this, a, P)
                    });
                    else q.mouseup(function (a) {
                        P.mouseDownHandler.call(this, a, P)
                    })
                }
            });
            return P
        }
    };
    a.fn.mapSvg = function (b) {
        if (P[b]) {
            return P[b].apply(this, Array.prototype.slice.call(arguments, 1))
        } else if (typeof b === "object") {
            return P.init.apply(this, arguments)
        } else if (!b) {
            return P
        } else {
            a.error("Method " + b + " does not exist on mapSvg plugin")
        }
    }
})(jQuery)