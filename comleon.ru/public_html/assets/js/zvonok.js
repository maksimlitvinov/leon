function checkForm(form) {
 var el,elName,value,type;
 var errorList = [];
var re = /^[0-9-+()\s]+$/;
 var em = /(.*)@(.*)\.(.*)$/i;
 var errorText = {
 1 : "Не заполнено поле \'Ваше имя\'",
 2 : "Не заполнено поле \'Номер телефона\'",
 4 : "Не заполнено поле \'E-mail\'",
 5 : "Исправьте ошибки в поле \'E-mail\'",
 6 : "Исправьте ошибки в поле \'Номер телефона\'"
 }
 for (var i = 0; i < form.elements.length; i++) {
 el = form.elements[i];
 elName = el.nodeName.toLowerCase();
 value = el.value;
 if (elName == "input") {
 type = el.type.toLowerCase();
 switch (type) {
 case "text" :
 if (el.name == "name" && value == "") errorList.push(1);
 if (el.name == "phone" && value == "") errorList.push(2);
 if (el.name == "phone" && !re.test(value) && value != "") errorList.push(6);
 if (el.name == "email" && value == "") errorList.push(4);
 if (el.name == "email" && !em.test(value) && value != "") errorList.push(5);
 
 break;
 }
 }  
 }
 if (!errorList.length) return true;
 var errorMsg = "При заполнении формы допущены следующие ошибки:\n\n";
 for (i = 0; i < errorList.length; i++) {
 errorMsg += errorText[errorList[i]] + "\n";
 }
 alert(errorMsg);
 return false;
}