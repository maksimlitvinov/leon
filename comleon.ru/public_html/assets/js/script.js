$(document).ready(function(){
	blockReg = ('calc-reg-ooo');
	
	$('#' + blockReg + ' input[type="text"]').keypress(function(event){
		var key, keyChar;
		if(!event) var event = window.event;
		if (event.keyCode) key = event.keyCode;
		else if(event.which) key = event.which;
		if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
		keyChar=String.fromCharCode(key);
		if(!/\d/.test(keyChar))	return false;
	});
	
	$('body').append('<div id="tooltip"></div>');

	$("[data-tooltip]").mousemove(function (eventObject) {

        $data_tooltip = $(this).attr("data-tooltip");   

        $("#tooltip").html($data_tooltip)

                     .css({ 

                         "top" : eventObject.pageY + 5,

                        "left" : eventObject.pageX + 5

                     })

                     .show();



    }).mouseout(function () {



        $("#tooltip").hide()

                     .html("")

                     .css({

                         "top" : 0,

                        "left" : 0

                     });

    });
	
	
	$('#' + blockReg + ' input').on('change', function() {
		var hidden = $(this).siblings('.hidden'), price=[], forForms=[], discount=0, discountCount = 0;
		if($(this).prop('checked')==true){
			hidden.css('display','block');
		}
		else {
			hidden.css('display','none');
		}
		$('#' + blockReg + ' input').each(function(){
			if($(this).parents().hasClass('hidden')==false || $(this).parents('.hidden').css('display')=='block') {
				var umn = 1, dataPrice = $(this).attr('data-price'),
				name = $(this).parent('div').children('label').text();
				if(dataPrice && $(this).prop('checked')==true){
					price.push(parseInt(dataPrice));
					forForms.push(name);
				}
				if($(this).attr('type')=='number'){
					if($(this).attr('data-action')=='multiply') umn=$(this).val();
					price.push(parseInt(dataPrice*umn));
					forForms.push(name + ' кол-во ' + umn);
				}
				if($(this).attr('data-action')=='discount' && $(this).prop('checked')) {
					discountCount=discountCount+1;
				}
			}
		});
		if(discountCount) {
			if(discountCount==4) {
				discount=500;
			}
			else if (discountCount>=5) {
				discount=1000;
			}
		}
		if(price.length) {
			var total = price.reduce(function(a, b) {
				return a + b;
			});
			total = total-discount;
			$('#end-price').text(total);
			$('#discount').text(discount);
			$('#modal-calc textarea').val(forForms.join("\n\n") + "\n\n\nОбщая стоимость: " + total + " руб.\n\n" + "Скидка: " + discount + " руб.");
		}
		else {
			$('#end-price').text('0');
			$('#discount').text('0');
		}
	});
	$('.b-zakaz').on('click',function() {
		$('#' + blockReg + ' input').trigger('change');
	});
	
});