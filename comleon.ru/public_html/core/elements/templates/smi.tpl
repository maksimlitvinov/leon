{extends 'file:templates/base.tpl'}

{block 'template'}
    <div class="cont-in-wr">
        <div class="cont-in">
            <div class="cont-l">
                <div class="bread">
                    {include 'crumbs'}
                </div>
                <h1><span>{$_modx->resource.pagetitle}</span></h1>
                {var $result = '!pdoResources' | snippet : [
                    'parents' => $_modx->resource.id,
                    'includeTVs' => 'image_cat,file',
                    'tpl' => '@FILE chunks/smi/item.tpl'
                ]}
                {if $result | length > 0 ?}
                    {$result}
                {else}
                    <div class="t-just">
                        {$_modx->resource.content}
                        <div class="clear"></div>
                    </div>
                {/if}
                {if $_modx->resource.rekvez ?}
                    <h2><span>Наши реквизиты</span></h2>
                    {$_modx->resource.rekvez}
                {/if}
                <div class="clear"></div>
                <div class="clear"></div>
            </div>
            <div class="cont-r">
                {include 'left_bar_about'}
            </div>
            <div class="clear"></div>
        </div>
    </div>
{/block}