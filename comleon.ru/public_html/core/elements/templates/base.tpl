{include 'meta'}
<body>
<div id="zvonok" class="reveal-modal medium">
    {'!AjaxForm' | snippet : [
        'form' => '@FILE chunks/forms/modal.tpl',
        'hooks' => 'notSpam,FormItSaveForm,email',
        'formFields' => 'name,phone',
        'fieldNames' => 'name==Имя отправителя,phone==Контактный телефон',
        'formName' => 'Заявка обратного звонка',
        'emailTpl' => '@FILE chunks/letters/modal.tpl',
        'emailTo' => ('5' | resource : 'email'),
        'emailSubject' => 'Заявка обратного звонка',
        'submitVar' => 'modal'
    ]}

</div>

{include 'header'}

{block 'template'}{/block}

{include 'footer'}
{include 'end_html'}