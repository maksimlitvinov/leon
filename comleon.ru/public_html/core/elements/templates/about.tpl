{extends 'file:templates/base.tpl'}

{block 'template'}
    <div class="cont-in-wr">
        <div class="cont-in">
            <div class="cont-l">
                <div class="bread">
                    {include 'crumbs'}
                </div>

                <h1><span>[[*pagetitle]]</span></h1>
                <div class="t-just">
                    [[*content]]
                    <div class="clear"></div>
                </div>
                [[*rekvez:notempty=`<h2><span>Наши реквизиты</span></h2>
                [[*rekvez]]`]]
                <div class="clear"></div>
                <div class="clear"></div>
                <div class="in-form">
                    <div class="forma-r">
                        [[$form_consalt]]
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="cont-r">
                [[$left_bar_about]]
            </div>
            <div class="clear"></div>
        </div>
    </div>
{/block}