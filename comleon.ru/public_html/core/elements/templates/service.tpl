{extends 'file:templates/base.tpl'}

{block 'template'}
    <div class="cont-in-wr">
        <div class="cont-in">
            <div class="cont-l">

                [[----- Если название страницы =, то на данной странице не отображаем данный блок -----]]
                [[-*pagetitle:is=`Бесплатная регистрация ООО`:then=``:else=`<div class="registr">
                    <a class="b-zakaz wid2" href="/[[~32]]">[[#32.pagetitle]]</a>
                </div>`]]

                [[----- Хлебные крошки -----]]
                <div class="bread">
                    [[$crumbs]]
                </div>

                <h1><span>[[*pagetitle]]</span></h1>
                <div class="t-just">
                    [[*content]]
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="clear"></div>
                <div class="in-form">
                    <div class="forma-r">
                        [[$form_consalt]]
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="cont-r">
                [[----- Если название страницы =, то на данной странице не отображаем данный блок -----]]
                [[-*pagetitle:is=`Бесплатная регистрация ООО`:then=``:else=`<div class="registr-left">
                    <a class="b-zakaz wid2" href="/[[~32]]">[[#32.pagetitle]]</a>
                </div>`]]

                [[$left_bar]]

            </div>
            <div class="clear"></div>
        </div>
    </div>
{/block}