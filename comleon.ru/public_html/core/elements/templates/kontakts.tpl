{extends 'file:templates/base.tpl'}

{block 'template'}
    <div class="cont-in-wr">
        <div class="map">
            <div class="map-desc">
                <p>
                    <span>[[*city]]<br>[[*address]]</span>&nbsp;
                    <span>Телефоны:</span>
                    <span>[[*phone1]]<br>[[*phone2]]</span><br>
                    <span>Время работы:</span> Пн.-Пт.: [[*pn-pt]]<br>
                    <span>Сб.-Вс.: [[*sb-vs]]</span><br>
                    <span>E-mail:</span>
                    <a href="mailto:[[*email]]">[[*email]]</a>
                </p>
            </div>
            <div class="maps">
                [[*map]]
            </div>
        </div>
    </div>
{/block}