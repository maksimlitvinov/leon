{extends 'file:templates/base.tpl'}

{block 'template'}
    <div class="pod-h">
        <div id="coin-slider-wr">
            <div class="slider-t">
                <div class="slider-t-in">
                    <span>{$_modx->resource.slide_text}</span>
                </div>
            </div>
            <div id="coin-slider">
                {'!Gallery' | snippet : [
                    'album' => $_modx->resource.gallery,
                    'thumbTpl' => 'gal_index_tpl',
                    'thumbWidth' => '1400',
                    'thumbHeight' => '389',
                    'imageWidth' => '1400',
                    'imageHeight' => '389'
                ]}
            </div>
        </div>


            <div class="menu">
                <div class="menu__block">
                    <div class="menu__wrap">
                        <div class="menu__left">
                            {'!pdoResources' | snippet : [
                                'parents' => '0',
                                'resources' => '6,7,8,9,52,70',
                                'tplWrapper' => '@INLINE <ul class="menu__items">{$output}</ul>',
                                'sortdir' => 'ASC',
                                'tpl' => '@INLINE <li class="menu__item"><a class="menu__item-link {$idx == 1 ? "active" : ""}" href="#menu{$id}">{$longtitle}</a></li>'
                            ]}
                        </div>
                        <div class="menu__right">
                            {'!pdoResources' | snippet : [
                                'parents' => '0',
                                'resources' => '6,7,8,9,52,70',
                                'includeContent' => '1',
                                'sortdir' => 'ASC',
                                'tpl' => '@FILE chunks/menu/menuContemt.tpl'
                            ]}
                        </div>
                    </div>
                </div>
            </div>

            {*<div class="tabs-wr">
                <div class="tabs">
                    {'!pdoMenu' | snippet : [
                        'parents' => '0',
                        'showHidden' => '1',
                        'resources' => '6,7,8,9,52',
                        'outputSeparator' => '4',
                        'tplOuter' => '@INLINE <ul class="tabset_tabs">{$wrapper}</ul>',
                        'tpl' => '@INLINE <li><a class="preActive postActive" href="{$link}">{$longtitle}<span></span></a></li>'
                    ]}
                </div>
                <div class="clear"></div>
            </div>*}

    </div>

    <div class="cont p-bot0">
        <div class="cont-l t-just">
            <h1><span>{'42' | resource : 'longtitle'}</span></h1>

            {set $news = $_modx->runSnippet('!pdoResources', [
                'parents' => 34,
                'includeTVs' => 'new_on_index',
                'includeContent' => 1,
                'tvPrefix' => '',
                'sortby' => 'publishedon',
                'sortdir' => 'DESC',
                'limit' => 1,
                'where' => '{"new_on_index:=":"1"}',
                'tpl' => '@INLINE {$content}'
            ])}
            {$news ?: $_modx->resource.content}
        </div>
        <div class="cont-r">
            <h2><span>{'4' | resource : 'longtitle'}</span></h2>
            <div class="custom-container vertical">
                <div class="carousel">
                    <ul>
                        {'!pdoResources' | snippet : [
                            'parents' => '4',
                            'depth' => '0',
                            'sortby' => 'menuindex',
                            'sortdir' => 'ASC',
                            'includeTVs' => 'image_cat, show_index',
                            'where' => '{"show_index:=":"1"}',
                            'tpl' => 'klient2_index_tpl'
                        ]}
                    </ul>
                </div>
            </div>
            {*<div>
                <a href="#">
                    <img src="[[++assets_url]]images/banner-1.jpg" alt="">
                </a>
            </div>
            <div>
                <a href="#">
                    <img src="[[++assets_url]]images/banner-2.jpg" alt="">
                </a>
            </div>*}
        </div>
        <div class="clear"></div>
    </div>

    <div class="cont p-top0">
            <div style="position: relative;" class="">

                <script src="{'assets_url' | config}js/script.js"></script>

                <h2><span>География работ</span></h2>

                {if $_modx->resource.geographiya ?}
                    <a href="#" title=""><img alt="" title="" src="{$_modx->resource.geographiya}"></a>
                {/if}

                <div id="map"></div>

                {include 'file:chunks/indexMap.tpl'}

                <div id="zvonok2" class="reveal-modal medium">
                    <h2><span>Заказать регистрацию</span></h2>
                    <form id="modal-calc" action="/zakaz.php" method="post">
                        <div>
                            <input class="check" name="check" type="hidden" value=""/>
                        </div>
                        <p>Представьтесь, пожалуйста:<span style="color:red">*</span><br /><input size="38"  type="text" name="name" /></p>
                        <p>Телефон:<span style="color:red">*</span><br /> <input size="38" type="text" name="phone" /></p>
                        <textarea style="visibility:hidden" name="comment"></textarea>
                        <div class="forma-r"><input class="forma-sub" name="submit" type="submit" value="Отправить"/></div>
                    </form>
                    <a class="close-reveal-modal">&#215;</a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="useful">
            <div class="useful-in">
                <!--noindex-->
                <div class="usef">
                    <h3><span>Полезные ссылки</span></h3>
                    <ul class="pol-ss">
                        {foreach $_modx->resource.link | fromJSON as $item}
                            <li>
                                <a rel="nofollow" target="_blank" title="{$item.title | replace : '"' : ''}" href="{$item.link}">{$item.title}</a>
                            </li>
                        {/foreach}
                    </ul>
                </div>
                <!--/noindex-->
                <div class="usef">
                    <h3><span>{('31' | resource : 'menutitle')?:('31' | resource : 'pagetitle')}</span></h3>
                    {'!pdoResources' | snippet : [
                        'parents' => '31',
                        'limit' => '2',
                        'tpl' => 'index_stati_tpl'
                    ]}
                </div>
                <div class="usef">
                    <h3><span>{('34' | resource : 'menutitle')?:('34' | resource : 'pagetitle')}</span></h3>
                    {'!pdoResources' | snippet : [
                        'parents' => '34',
                        'limit' => '3',
                        'tpl' => 'index_news_tpl'
                    ]}
                </div>
            </div>
            <div class="clear"></div>
        </div>
{/block}