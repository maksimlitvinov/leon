{ignore}
    <script type="text/javascript">

        var height = 600;
        if (document.body.clientWidth <= 360) {
            height = 240;
        }

        $('#map').mapSvg({
            source: '/rs2.svg',
            tooltipsMode: "combined",
            zoom: 0,
            pan: 0,
            loadingText : 'Загрузка карты...',
            colors: {                        // Colors for each region state
                base: "#549C1F",
                background: "#transparent", // Enter "transparent" for transparent background
                hover: "#0A6E31",
                selected: "#0A6E31",
                disabled: "#ffffff",
                stroke: "#989898"
            },
        viewBox: [-40.02605388552581, 301.3819103583459, 1208.4645061728395, 83.954475308642],
            hover_mode: "brightness",
            selected_mode: "brightness",
            hover_brightness: 0.6,
            selected_brightness: 0.4,
            zoomLimit: [-14, 5],
            cursor:        'pointer',
            width:         1000,
            height:        height,
            regions: {

            {*
                #C8E1A8 - Green
                #AEE4EB - lightblue
                #CBBDD7 - Fiolet
                #F0D5A8 - Orange
                #F3DAA4 - OrangeTrue
                #D0A6AA - DarchPink
                #F4C4AD - Pink
                #9BCBBD - DarkGreen
            *}

            'tu92': { attr:{ fill: "#F3DAA4" }, popover: '<b>Город федерального значения Севастополь</b><br/>', tooltip: "Город федерального значения Севастополь" },
            'tu82': { attr:{ fill: "#F3DAA4" }, popover: '<h4>Республика Крым</h4><p><a title="Перейти на страницу [[#98.menutitle:empty=`[[#98.pagetitle]]`]]" href="[[#98.uri]]"><b>[[#98.menutitle:empty=`[[#98.pagetitle]]`]]</b></a></p>', tooltip: "Республика Крым" },
            'tu22': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Алтайский край</h4>', tooltip: "Алтайский край" },
            'tu28': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Амурская область</h4>', tooltip: "Амурская область" },
            'tu29': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Архангельская область</h4><p><a title="Перейти на страницу [[#39.menutitle:empty=`[[#39.pagetitle]]`]]" href="[[#39.uri]]"><b>[[#39.menutitle:empty=`[[#39.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#97.menutitle:empty=`[[#97.pagetitle]]`]]" href="[[#97.uri]]"><b>[[#97.menutitle:empty=`[[#97.pagetitle]]`]]</b></a></p>', tooltip: "Архангельская область" },
            'tu30': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Астраханская область</h4>', tooltip: "Астраханская область" },
            'tu31': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Белгородская область</h4>', tooltip: "Белгородская область" },
            'tu32': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Брянская область</h4>', tooltip: "Брянская область" },
            'tu34': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Волгоградская область</h4>', tooltip: "Волгоградская область" },
            'tu35': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Вологодская область</h4><p><a title="Перейти на страницу [[#50.menutitle:empty=`[[#50.pagetitle]]`]]" href="[[#50.uri]]"><b>[[#50.menutitle:empty=`[[#50.pagetitle]]`]]</b></a></p><p><a title="Перейти на страницу [[#57.menutitle:empty=`[[#57.pagetitle]]`]]" href="[[#57.uri]]"><b>[[#57.menutitle:empty=`[[#57.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#39.menutitle:empty=`[[#39.pagetitle]]`]]" href="[[#39.uri]]"><b>[[#39.menutitle:empty=`[[#39.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#89.menutitle:empty=`[[#89.pagetitle]]`]]" href="[[#89.uri]]"><b>[[#89.menutitle:empty=`[[#89.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#94.menutitle:empty=`[[#94.pagetitle]]`]]" href="[[#94.uri]]"><b>[[#94.menutitle:empty=`[[#94.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#56.menutitle:empty=`[[#56.pagetitle]]`]]" href="[[#56.uri]]"><b>[[#56.menutitle:empty=`[[#56.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#91.menutitle:empty=`[[#91.pagetitle]]`]]" href="[[#91.uri]]"><b>[[#91.menutitle:empty=`[[#91.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#92.menutitle:empty=`[[#92.pagetitle]]`]]" href="[[#92.uri]]"><b>[[#92.menutitle:empty=`[[#92.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#71.menutitle:empty=`[[#71.pagetitle]]`]]" href="[[#71.uri]]"><b>[[#71.menutitle:empty=`[[#71.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#90.menutitle:empty=`[[#90.pagetitle]]`]]" href="[[#90.uri]]"><b>[[#90.menutitle:empty=`[[#90.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#77.menutitle:empty=`[[#77.pagetitle]]`]]" href="[[#77.uri]]"><b>[[#77.menutitle:empty=`[[#77.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#99.menutitle:empty=`[[#99.pagetitle]]`]]" href="[[#99.uri]]"><b>[[#99.menutitle:empty=`[[#99.pagetitle]]`]]</b></a></p>', tooltip: "Вологодская область" },
            'tu36': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Воронежская область</h4>', tooltip: "Воронежская область" },
            'tu77': { attr:{ fill: "#F3DAA4" } , popover: '<p><a title="Перейти на страницу [[#75.menutitle:empty=`[[#75.pagetitle]]`]]" href="[[#75.uri]]"><b>[[#75.menutitle:empty=`[[#75.pagetitle]]`]]</b></a></p>', tooltip: "Город федерального значения Москва" },
            'tu78': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Город федерального значения Санкт-Петербург</h4>', tooltip: "Город федерального значения Санкт-Петербург" },
            'tu79': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Еврейская автономная область</h4>', tooltip: "Еврейская автономная область" },
            'tu75': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Забайкальский край</h4>', tooltip: "Забайкальский край" },
            'tu37': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Ивановская область</h4>', tooltip: "Ивановская область" },
            'tu38': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Иркутская область</h4>', tooltip: "Иркутская область" },
            'tu07': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Кабардино-Балкарская Республика</h4>', tooltip: "Кабардино-Балкарская Республика" },
            'tu39': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Калининградская область</h4>', tooltip: "Калининградская область" },
            'tu40': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Калужская область</h4>', tooltip: "Калужская область" },
            'tu41': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Камчатский край</h4>', tooltip: "Камчатский край" },
            'tu09': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Карачаево-Черкесская Республика</h4>', tooltip: "Карачаево-Черкесская Республика" },
            'tu42': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Кемеровская область</h4>', tooltip: "Кемеровская область" },
            'tu43': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Кировская область</h4>', tooltip: "Кировская область" },
            'tu44': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Костромская область</h4>', tooltip: "Костромская область" },
            'tu23': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Краснодарский край</h4>', tooltip: "Краснодарский край" },
            'tu24': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Красноярский край</h4>', tooltip: "Красноярский край" },
            'tu45': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Курганская область</h4>', tooltip: "Курганская область" },
            'tu46': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Курская область</h4>', tooltip: "Курская область" },
            'tu47': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Ленинградская область</h4><p><a title="Перейти на страницу [[#87.menutitle:empty=`[[#87.pagetitle]]`]]" href="[[#87.uri]]"><b>[[#87.menutitle:empty=`[[#87.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#73.menutitle:empty=`[[#73.pagetitle]]`]]" href="[[#73.uri]]"><b>[[#73.menutitle:empty=`[[#73.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#90.menutitle:empty=`[[#90.pagetitle]]`]]" href="[[#90.uri]]"><b>[[#90.menutitle:empty=`[[#90.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#89.menutitle:empty=`[[#89.pagetitle]]`]]" href="[[#89.uri]]"><b>[[#89.menutitle:empty=`[[#89.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#100.menutitle:empty=`[[#100.pagetitle]]`]]" href="[[#100.uri]]"><b>[[#100.menutitle:empty=`[[#100.pagetitle]]`]]</b></a></p>', tooltip: "Ленинградская область" },
            'tu48': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Липецкая область</h4>', tooltip: "Липецкая область" },
            'tu49': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Магаданская область</h4>', tooltip: "Магаданская область" },
            'tu50': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Московская область</h4><p><a title="Перейти на страницу [[#58.menutitle:empty=`[[#58.pagetitle]]`]]" href="[[#58.uri]]"><b>[[#58.menutitle:empty=`[[#58.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#38.menutitle:empty=`[[#38.pagetitle]]`]]" href="[[#38.uri]]"><b>[[#38.menutitle:empty=`[[#38.pagetitle]]`]]</b></a></p>', tooltip: "Московская область" },
            'tu51': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Мурманская область</h4><p><a title="Перейти на страницу [[#40.menutitle:empty=`[[#40.pagetitle]]`]]" href="[[#40.uri]]"><b>[[#40.menutitle:empty=`[[#40.pagetitle]]`]], Мурманский филиал</b></a></p> <p><a title="Перейти на страницу [[#73.menutitle:empty=`[[#73.pagetitle]]`]]" href="[[#73.uri]]"><b>[[#73.menutitle:empty=`[[#73.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#75.menutitle:empty=`[[#75.pagetitle]]`]]" href="[[#75.uri]]"><b>[[#75.menutitle:empty=`[[#75.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#95.menutitle:empty=`[[#95.pagetitle]]`]]" href="[[#95.uri]]"><b>[[#95.menutitle:empty=`[[#95.pagetitle]]`]]</b></a></p>', tooltip: "Мурманская область" },
            'tu83': { attr:{ fill: "#C8E1A8" } , popover: '<h4Ненецкий автономный округ</h4>', tooltip: "Ненецкий автономный округ" },
            'tu52': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Нижегородская область</h4>', tooltip: "Нижегородская область" },
            'tu53': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Новгородская область</h4><p><a title="Перейти на страницу [[#38.menutitle:empty=`[[#38.pagetitle]]`]]" href="[[#38.uri]]"><b>[[#38.menutitle:empty=`[[#38.pagetitle]]`]]</b></a></p>', tooltip: "Новгородская область" },
            'tu54': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Новосибирская область</h4>', tooltip: "Новосибирская область" },
            'tu55': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Омская область</h4>', tooltip: "Омская область" },
            'tu56': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Оренбургская область</h4><p><a title="Перейти на страницу [[#86.menutitle:empty=`[[#86.pagetitle]]`]]" href="[[#86.uri]]"><b>[[#86.menutitle:empty=`[[#86.pagetitle]]`]]</b></a></p>', tooltip: "Оренбургская область" },
            'tu57': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Орловская область</h4>', tooltip: "Орловская область" },
            'tu58': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Пензенская область</h4><p><a title="Перейти на страницу [[#86.menutitle:empty=`[[#86.pagetitle]]`]]" href="[[#86.uri]]"><b>[[#86.menutitle:empty=`[[#86.pagetitle]]`]]</b></a></p>', tooltip: "Пензенская область" },
            'tu59': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Пермский край</h4>', tooltip: "Пермский край" },
            'tu25': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Приморский край</h4>', tooltip: "Приморский край" },
            'tu60': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Псковская область</h4>', tooltip: "Псковская область" },
            'tu05': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Дагестан</h4>', tooltip: "Республика Дагестан" },
            'tu01': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Адыгея</h4>', tooltip: "Республика Адыгея" },
            'tu04': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Алтай</h4>', tooltip: "Республика Алтай" },
            'tu02': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Башкортостан</h4>', tooltip: "Республика Башкортостан" },
            'tu03': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Бурятия</h4>', tooltip: "Республика Бурятия" },
            'tu06': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Ингушетия</h4>', tooltip: "Республика Ингушетия" },
            'tu08': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Калмыкия</h4>', tooltip: "Республика Калмыкия" },
            'tu10': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Республика Карелия</h4><p><a title="Перейти на страницу [[#40.menutitle:empty=`[[#40.pagetitle]]`]]" href="[[#40.uri]]"><b>[[#40.menutitle:empty=`[[#40.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#75.menutitle:empty=`[[#75.pagetitle]]`]]" href="[[#75.uri]]"><b>[[#75.menutitle:empty=`[[#75.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#95.menutitle:empty=`[[#95.pagetitle]]`]]" href="[[#95.uri]]"><b>[[#95.menutitle:empty=`[[#95.pagetitle]]`]]</b></a></p>', tooltip: "Республика Карелия" },
            'tu11': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Коми</h4>', tooltip: "Республика Коми" },
            'tu12': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Марий Эл</h4>', tooltip: "Республика Марий Эл" },
            'tu13': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Мордовия</h4>', tooltip: "Республика Мордовия" },
            'tu14': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Саха (Якутия)</h4>', tooltip: "Республика Саха (Якутия)" },
            'tu15': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Северная Осетия-Алания</h4>', tooltip: "Республика Северная Осетия-Алания" },
            'tu16': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Татарстан</h4>', tooltip: "Республика Татарстан" },
            'tu19': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Хакасия</h4>', tooltip: "Республика Хакасия" },
            'tu61': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Ростовская область</h4>', tooltip: "Ростовская область" },
            'tu62': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Рязанская область</h4>', tooltip: "Рязанская область" },
            'tu63': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Самарская область</h4><p><a title="Перейти на страницу [[#86.menutitle:empty=`[[#86.pagetitle]]`]]" href="[[#86.uri]]"><b>[[#86.menutitle:empty=`[[#86.pagetitle]]`]]</b></a></p>', tooltip: "Самарская область" },
            'tu64': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Саратовская область</h4>', tooltip: "Саратовская область" },
            'tu65': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Сахалинская область</h4>', tooltip: "Сахалинская область" },
            'tu66': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Свердловская область</h4>', tooltip: "Свердловская область" },
            'tu67': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Смоленская область</h4>', tooltip: "Смоленская область" },
            'tu26': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Ставропольский край</h4>', tooltip: "Ставропольский край" },
            'tu68': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Тамбовская область</h4>', tooltip: "Тамбовская область" },
            'tu69': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Тверская область</h4><p><a title="Перейти на страницу [[#38.menutitle:empty=`[[#38.pagetitle]]`]]" href="[[#38.uri]]"><b>[[#38.menutitle:empty=`[[#38.pagetitle]]`]]</b></a></p>', tooltip: "Тверская область" },
            'tu70': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Томская область</h4>', tooltip: "Томская область" },
            'tu71': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Тульская область</h4>', tooltip: "Тульская область" },
            'tu72': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Тюменская область</h4>', tooltip: "Тюменская область" },
            'tu18': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Удмуртская Республика</h4>', tooltip: "Удмуртская Республика" },
            'tu73': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Ульяновская область</h4>', tooltip: "Ульяновская область" },
            'tu27': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Хабаровский край</h4>', tooltip: "Хабаровский край" },
            'tu86': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Ханты-Мансийский автономный округ — Югра</h4>', tooltip: "Ханты-Мансийский автономный округ — Югра" },
            'tu74': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Челябинская область</h4>', tooltip: "Челябинская область" },
            'tu20': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Чеченская Республика</h4>', tooltip: "Чеченская Республика" },
            'tu21': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Чувашская Республика</h4>', tooltip: "Чувашская Республика" },
            'tu87': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Чукотский автономный округ</h4>', tooltip: "Чукотский автономный округ" },
            'tu89': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Ямало-Ненецкий автономный округ</h4>', tooltip: "Ямало-Ненецкий автономный округ" },
            'tu76': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Ярославская область</h4><p><a title="Перейти на страницу [[#93.menutitle:empty=`[[#93.pagetitle]]`]]" href="[[#93.uri]]"><b>[[#93.menutitle:empty=`[[#93.pagetitle]]`]]</b></a></p> <p><a title="Перейти на страницу [[#96.menutitle:empty=`[[#96.pagetitle]]`]]" href="[[#96.uri]]"><b>[[#96.menutitle:empty=`[[#96.pagetitle]]`]]</b></a></p>', tooltip: "Ярославская область" },
            'tu33': { attr:{ fill: "#F3DAA4" } , popover: '<h4>Владимирская область</h4><p><a title="Перейти на страницу [[#96.menutitle:empty=`[[#96.pagetitle]]`]]" href="[[#96.uri]]"><b>[[#96.menutitle:empty=`[[#96.pagetitle]]`]]</b></a></p>', tooltip: "Владимирская область" },
            'tu17': { attr:{ fill: "#C8E1A8" } , popover: '<h4>Республика Тыва</h4>', tooltip: "Республика Тыва" }
        }

        });
    </script>
{/ignore}